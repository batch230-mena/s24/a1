//console.log("hello world");

let num = 2;
let getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`);

//----------------------------------------

const address = ["258 Washington Ave NW", "California", 90011];
const [street, state, zipCode] = address;
console.log(`I live at ${street}, ${state} ${zipCode}`);

//----------------------------------------

const animal = {
	name: "Lolong",
	kind: "saltwater crocodile",
	weight: "1075 kgs",
	height: "20 ft 3 in"
};
const {name, kind, weight, height} = animal;
console.log(`${name} was a ${kind}. He weighed at ${weight} with a measurement of ${height}.`);

//--hula----------------------------------

const numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
	console.log(number);
});

//--hula----------------------------------

let i = 0;
let reduceNumber = numbers.reduce(function(acc,cur){
	++i;
	return acc + cur;
});

console.log(reduceNumber);

//----------------------------------------

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const newDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(newDog);
